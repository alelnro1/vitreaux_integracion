<?php

/**
 * Se busca en la tabla importaciones el último valor de secuencia insertado. Ej: 6
 * @param $mysql
 * @return mixed
 */
function buscarUltimoSecuencia($mysql)
{
    // Busco la ultima fila ordenada por secuencia
    $ultima_fila = $mysql->select('importaciones', '', 'secuencia DESC', '1');

    // Si no hay ultima fila => estamos en la primera vez => devuelvo el 0
    if (empty($ultima_fila)) {
        return 0;
    } else {
        return $ultima_fila['secuencia'];
    }
}


/**
 * Se busca el archivo en el directorio.
 * $ultimo_secuencia viene 006 => vamos a buscar el 007
 * @param $ultimo_secuencia
 * @return bool|string
 */
function siguienteArchivoABuscar($ultimo_secuencia)
{
    // Defino al siguiente a partir del ultimo. Lo parseo por las dudas
    $siguiente_secuencia = (int)$ultimo_secuencia + 1;

    $siguiente_archivo = 'exportacion_' . $siguiente_secuencia . '.txt';

    // Busco el archivo a procesar
    $archivos = glob(DIRECTORIO_INTEGRACION_IN . $siguiente_archivo);

    // Si encontre un archivo => lo devuelvo
    if (count($archivos) > 0) {
        return $siguiente_archivo;
    }

    // No se encontró un archivo que siga con la secuencia => devuelvo false
    return false;
}

/**
 * Se procesa el archivo
 * Cada línea viene dada por [nombre_tabla];[id_sg];[campo1];[campo2];[campoN]
 * @param $nombre_archivo
 * @param $mysql
 * @return bool
 */
function procesarImportacionIn($nombre_archivo, $mysql)
{
    $log_sql = file_get_contents(LOG_SQL);
    $log_sql .= "\n\r" . date('d/m/Y', time()) . ' a las ' . date('H:i', time()) . PHP_EOL ;
    $log_sql .='>>>> NOMBRE DE ARCHIVO: '. $nombre_archivo . PHP_EOL ;
    file_put_contents(LOG_SQL, $log_sql);
    // Leo el archivo y me lo da como array
    $lineas = file(DIRECTORIO_INTEGRACION_IN . $nombre_archivo);
    // Lo recorro linea por linea
    foreach ($lineas as $key => $linea) {
        $log_sql = file_get_contents(LOG_SQL);
        $log_sql .= '>> Linea:' . $key . PHP_EOL ;
        file_put_contents(LOG_SQL, $log_sql);

        $palabras = explode(";", $linea);
        // Copio las palabras a las values, para poder eliminar el nombre de la tabla, y separarla de los campos.
        $values = $palabras;

        // Elimino el nombre de la tabla de las values
        unset($values[0]);
        // Obtengo el nombre de la tabla
        $tabla = $palabras[0];
        $campos = camposDeTabla($tabla, $mysql);

        if (actualizarDatos($tabla, $campos, $values, $mysql)) {
            /* Escribir al final de la linea del archivo OK*/
            $resultado = "OK";
        } else {
            /* Escribir al final de la linea del archivo NOK*/
            $resultado = "NOK";
        }

        // Inserto el resultado adelante del array
        array_unshift($palabras, $resultado);

        // Paso el Array a texto
        $lineas[$key] = implode(';', $palabras);

    }
    //Grabo el archivo
    file_put_contents(DIRECTORIO_INTEGRACION_IN . $nombre_archivo, $lineas);

    return true;
}


/**
 * Solicita los nombres de los campos de la tabla que recibe.
 * @param $tabla
 * @param $mysql
 * @return mixed
 */
function camposDeTabla($tabla, $mysql)
{
    $campos = [];
    $query = 'SHOW COLUMNS FROM ' . $tabla;
    $resultados = $mysql->executeSQL($query);

    foreach ($resultados as $resultado) {
        array_push($campos, $resultado['Field']);
    }
    return $campos;
}


/**
 * Actualiza los datos de las tablas correspondientes a cada linea que le va llegando.
 * @param $tabla
 * @param $campos
 * @param $values
 * @param $mysql
 * @return bool
 */
function actualizarDatos($tabla, $campos = null, $values, $mysql)
{
    // Falta hacer
    array_pop($values);
    $parametros_update = [];
    $parametros_insert = [];
    $id_sg = $values[1];
    $query_existe = 'SELECT * FROM '. $tabla .' WHERE id_sg =' .$id_sg;
    //echo($query . '<br>');

    if (count($mysql->executeSQL($query_existe)) != 1) {
        //update
        foreach ($values as $key => $value) {
            if ($key != 1) {
                $cadena = $campos[$key - 1] . '= "' . $value . '"';
                array_push($parametros_update, $cadena);
            }
        }

        $string = implode(",", $parametros_update);
        $query = 'UPDATE ' . $tabla . ' SET ' . $string . ' WHERE id_sg=' . $id_sg;
        //echo($query . '<br>');
    } else {
        //insert
        unset($campos[0] );

        foreach ($values as $key => $value) {
            if ($key == 1) {
                $id_sg = $value;
            } else {
                $cadena = '"' . $value .'"';
                array_push($parametros_insert, $cadena);
            }
        }

        $campos_string = implode(",", $campos);
        $values_string = implode(",",$parametros_insert);
        $query = 'INSERT INTO ' . $tabla . ' (' . $campos_string . ') VALUES ('. $values_string .',' . $id_sg . ')';
        //echo($query . '<br>');
    }

    if($mysql->executeSQL($query)){
        // Guardamos en el log_sql el error de que no se pudo procesar la linea del archivo.
        $log_sql = file_get_contents(LOG_SQL);
        $log_sql .= 'OK --->' .$query .  PHP_EOL ;
        $retorno = true;

    }
    else{
        // Guardamos en el log_sql el error de que no se pudo procesar la linea del archivo.
            $log_sql = file_get_contents(LOG_SQL);
            $log_sql .= 'NOK -->' .$query .  PHP_EOL ;
            $retorno = false;

    }
    file_put_contents(LOG_SQL, $log_sql);

    return $retorno;

}
