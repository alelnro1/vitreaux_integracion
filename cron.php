<?php

/**
 * Incluyo los archivos de configuración
 */
include_once('defines.php');
include_once('mysql.class.php');
include_once('functions.php');

// Verifico que haya al menos un archivo nuevo en el directorio de integracion_in
$fi = new FilesystemIterator(DIRECTORIO_INTEGRACION_IN, FilesystemIterator::SKIP_DOTS);

if (iterator_count($fi) > 0) {
    // Creo la conexión a la base de datos
    $mysql = new MySQL(MYSQL_NAME, MYSQL_USER, MYSQL_PASS, MYSQL_HOST);

    // Hay al menos un archivo => vamos a recorrerlos hasta que no hayan mas
    while (iterator_count($fi) > 0) {
        // Busco el ultimo numero de secuencia. Ej: 6
        $ultimo_secuencia = buscarUltimoSecuencia($mysql);

        // Voy a buscar el siguiente archivo de secuencia. Ej: 7
        $archivo_siguiente = siguienteArchivoABuscar($ultimo_secuencia);

        /*
         * Si hay archivos, y ninguno matchea al siguiente (Ej 6)=> envio un mail de error
         * Si hay archivos y matchea al siguiente (Ej 7) => proceso la importacion
         */
        if ($archivo_siguiente) {
            // Proceso el archivo y me devuelve solo el
            $archivo_procesado = procesarImportacionIn($archivo_siguiente, $mysql);

            // Si se termina de procesar el archivo lo elimino y sigo con el siguiente
            if ($archivo_procesado) {
                // Muevo el archivo al directorio out
                rename(DIRECTORIO_INTEGRACION_IN . $archivo_siguiente, DIRECTORIO_INTEGRACION_OUT . $archivo_siguiente);

                $next = ((int)$ultimo_secuencia + 1);
                $date = date('Y-m-d', strtotime("now"));

                // Inserto el archivo y numero de secuencia recien procesados
                $query = "INSERT INTO importaciones (nombre_archivo, secuencia, fecha) VALUES ('" . $archivo_siguiente . "', '" . $next . "', '" . $date ."')";
                $mysql->executeSQL($query);
            }

            // Corto la ejecucion
            break;
        } else {
            // Guardamos en el log el error de que no se pudo procesar el archivo
            $log = file_get_contents(LOG);
            $log .= 'No se encuentra el archivo con secuencia ' . ($ultimo_secuencia + 1) . '. El error ocurrió el dia ' . date('d/m/Y', time()) . ' a las ' . date('H:i', time()) . PHP_EOL;

            // Insert
            file_put_contents(LOG, $log);

            // Cortamos la ejecución porque no se van a borrar los archivos
            break;
        }
    }
}